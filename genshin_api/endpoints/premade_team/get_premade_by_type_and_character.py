from genshin_api import app, mongo

@app.route('/genshin-api/v1/premade-team/<type>/<character>/', methods=['GET'])
def api_get_premade_team_type_by_character(type, character):
    result = mongo.db.team_by_type.find()
    response={"teams-possibles" : []}
    liste = []
    
    for element in result:
        for key, value in element.items():
            if key != "_id" and key not in liste:
                if value["type"] == type:
                    for ele in value["team-possible"]:
                        for key_name, name in ele.items():
                            if name == character:
                                temp={"name" : key, "team-possible" : value["team-possible"] }
                                liste.append(temp)
    response["teams-possibles"] = liste
    return response
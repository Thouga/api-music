from genshin_api import app, mongo

@app.route('/genshin-api/v1/premade-team/<type>/<name>/', methods=['DELETE'])
def api_delete_premade_team_type_name(type, name):
    result = mongo.db.team_by_type.find()

    response = {
        "team-deleted": []
    }
    team_deleted = {}

    for team in result:
        for key, value in team.items():
            if key == "_id":
                to_del_id = value
            elif value["ajout"]=="user" and value["type"]==type:
                for team_possible in value["team-possible"]:
                    team_deleted = team_possible
                    mongo.db.team_by_type.delete_one({"_id":to_del_id})
    print(team_deleted)
    response["team-deleted"].append(team_deleted)
    return response

from genshin_api import app, mongo
from flask import request
import json

@app.route('/genshin-api/v1/premade-team/<type>/<name>/', methods=['POST'])
def api_add_premade_team_type_name(type, name):
    # TODO 
    data = json.loads(request.data)
    response = {
        "team-added": []
    }
    team_added = {}
    found = False
    team_by_type =  mongo.db.team_by_type.find()
    for element in team_by_type:
        for key, values in element.items():
            if name == key:
                found = True
                response["team-added"].append("Equipe deja existante")
                break
    if not found:
        team_added = data
        mongo.db.team_by_type.insert({
            name:{
                "type": type,
                "ajout":"user",
                "team-possible": [data]
            }
        })
    response["team-added"].append(team_added)
    return response

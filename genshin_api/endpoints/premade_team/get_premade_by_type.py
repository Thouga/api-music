from genshin_api import app, mongo

@app.route('/genshin-api/v1/premade-team/<type>', methods=['GET'])
def api_get_premade_team_type(type):
    result = mongo.db.team_by_type.find()
    response={"teams-possibles" : []}
    liste = []
    
    for element in result:
        for key, value in element.items():
            if key != "_id":
                if value["type"] == type:
                    temp={"name" : key, "team-possible" : value["team-possible"] }
                    liste.append(temp)
    response["teams-possibles"] = liste
    return response


from genshin_api import app, mongo
from flask import request
import json

@app.route('/genshin-api/v1/premade-team/<type>/<name>/', methods=['PUT'])
def api_modify_premade_team_type_name(type, name):
    # TODO 
    data = json.loads(request.data)
    response = {"name": "nom d'equipe", "edit": "equipe editee"}
    team_by_type =  mongo.db.team_by_type.find()
    for element in team_by_type:
        for key_team, values_team in element.items():
            if key_team == "_id":
                to_edit_id = values_team
            elif values_team["ajout"] == "user" and values_team["type"] == type:
                print(values_team["team-possible"])
                mongo.db.team_by_type.update_one({"_id": to_edit_id},{"$set":{
                    key_team: {
                        "type": type,
                        "ajout":"user",
                        "team-possible":[data]
                    }
                    }})
    response["name"] = name
    response["edit"] = data
    return response

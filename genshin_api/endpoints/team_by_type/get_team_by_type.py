from genshin_api import app, mongo
import requests

@app.route('/genshin-api/v1/team-by-type/<type>/<element>/', methods=['GET'])
def api_team_by_type(type, element):
    # TODO 
    liste =  []
    response = {"type": "dps ou support", "element": "pyro ou cryo etc", "personnages":[]}
    result = mongo.db.team_type_by_element.find()
    for everything in result:
        for element_key, element_values in everything.items():
            if element_key != '_id' and element_key == element:
                for character_type_key, character_type_values in element_values.items():
                    if character_type_key == "main-dps" and character_type_key == type:
                        for character_role in character_type_values:
                            for key, value in character_role.items():
                                if key == "main-dps" and value not in liste:
                                    liste.append(value)
                    elif character_type_key == "support-dps" and character_type_key == type:
                        for character_role in character_type_values:
                            for key, value in character_role.items():
                                if key == "support-dps" and value not in liste:
                                    liste.append(value)
                    elif character_type_key == "support" and character_type_key == type:
                        for character_role in character_type_values:
                            for key, value in character_role.items():
                                if key == "support" and value not in liste:
                                    liste.append(value)
                    elif character_type_key == "healer" and character_type_key == type:
                        for character_role in character_type_values:
                            for key, value in character_role.items():
                                if key == "healer" and value not in liste:
                                    liste.append(value)
    
    response["type"] = type
    response["element"] = element
    response["personnages"] = liste
    return response
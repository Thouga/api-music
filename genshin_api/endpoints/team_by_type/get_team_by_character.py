from genshin_api import app, mongo
import requests

@app.route('/genshin-api/v1/team-by-type/<character>', methods=['GET'])
def api_team_character(character):
    url = "https://api.genshin.dev/"
    # interroger l'api non officielle pour demander
    # l'élément du personnage <character> demandé 
    content_character = requests.get(url+"characters/"+character)
    data = content_character.json()
    liste =  []
    response = {"description de " + character: "description" , "team_existante": []}

    result = mongo.db.team_type_by_element.find()
    for everything in result:
        for element_key, element_values in everything.items():
            if element_key != '_id':
                for character_type_key, character_type_values in element_values.items():
                    for character_role in character_type_values:
                        for key, value in character_role.items():
                            if value == character:
                                if character_role not in liste:
                                    liste.append(character_role)
                                content_character_desc = data["description"]
    response["description de " + character] = content_character_desc
    response["team_existante"] = liste
    return response

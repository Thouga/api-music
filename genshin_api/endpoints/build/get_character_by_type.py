from genshin_api import app, mongo
import requests
import copy

@app.route('/genshin-api/v1/build/type/<character>/<type>/', methods=['GET'])
def api_get_build_character_by_type(character, type):
    response = {"build-possible-"+type: "build"}

    url = "https://api.genshin.dev/"
    query = {"name": character}
    result = mongo.db.characters.find_one(query)
    build_possible = result["build_possible"][0]
    
    if build_possible.get(type):
        build_copy = copy.deepcopy(build_possible[type])
        for setbuild in build_copy:
            # Partie description des armes
            _get_weapon_description(url, setbuild)
            # Partie description des artéfacts
            for setartefact in setbuild["set-artefact"]:
                _get_artifact_description(url, setartefact)
        response["build-possible-"+type]=build_copy
        return response
    else:            
        return "Ce personnage n'est pas joué de cette façon-là, essaie un autre type de build !"


def _get_weapon_description(url, setbuild):
    content_weapon=requests.get(url+"weapons/"+setbuild["arme"])
    data=content_weapon.json()
    description=data["passiveDesc"]
    setbuild["description-weapon"] = description

def _get_artifact_description(url, setartefact):
    content_artifact_one=requests.get(url+"artifacts/"+setartefact["set-artefact-un"])
    content_artifact_two=requests.get(url+"artifacts/"+setartefact["set-artefact-deux"])
    data_one=content_artifact_one.json()
    data_two=content_artifact_two.json()
    
    if setartefact["set-artefact-un"] == setartefact["set-artefact-deux"]:
        description=data_one["4-piece_bonus"]
        setartefact["description"] = description
    elif setartefact["set-artefact-un"] != setartefact["set-artefact-deux"]:
        description_one=data_one["2-piece_bonus"]
        description_two=data_two["2-piece_bonus"]
        setartefact["description-first-artefact"] = description_one
        setartefact["description-second-artefact"] = description_two

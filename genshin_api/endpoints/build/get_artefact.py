from genshin_api import app, mongo
import requests

@app.route('/genshin-api/v1/build/artefact/<artefact>/', methods=['GET'])
def api_get_character_for_artefact(artefact):
    response = {
        "list_of_characters" : []
    }
    list_character = []

    url = "https://api.genshin.dev/characters/"
    result = mongo.db.characters.find()
    for character in result:
        for type_chara in character["build_possible"]:
            for key, value in type_chara.items():
                for single_value in value:
                    for setarte in single_value["set-artefact"]:
                        for key, value in setarte.items():
                            if value == artefact and character["name"] not in list_character:
                                list_character.append(character["name"])
    for perso in list_character:
        _creation_response(url, perso, response)
    
    return response

def _creation_response(url, perso, response):
    dict_temp = {
        "name": "test",
        "description": "description",
        "element": "element"
    }
    # je vais chercher les infos description et type
    content_character=requests.get(url+perso)
    data=content_character.json()
    description=data["description"]
    type_chara = data["vision"]
    # je remplace dans mon dict
    dict_temp["name"]=perso
    dict_temp["description"]= description
    dict_temp["element"] = type_chara

    response["list_of_characters"].append(dict_temp)
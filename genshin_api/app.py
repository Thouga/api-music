from genshin_api import app, mongo
from flask_swagger_ui import get_swaggerui_blueprint
import database

@app.route('/', methods=['GET'])
def home():
    return '''<h1>Helper on Genshin Impact</h1>
<p>An API used to help compose your team and how to build your characters.</p>'''

if __name__ == "__main__":
    SWAGGER_URL = '/swagger'  # URL for exposing Swagger UI (without trailing '/')
    API_URL = '/static/swagger.json'  # Our API url (can of course be a local resource)

    # Call factory function to create our blueprint
    swaggerui_blueprint = get_swaggerui_blueprint(
        SWAGGER_URL,  # Swagger UI static files will be mapped to '{SWAGGER_URL}/dist/'
        API_URL,
        config={  # Swagger UI config overrides
            'app_name': "Genshin-Impact-API"
        }
    )

    app.register_blueprint(swaggerui_blueprint)
    database.implement_database(mongo)
    app.run(debug=True, use_reloader=False)

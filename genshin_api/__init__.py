import flask
from flask import request, jsonify
from flask_pymongo import PyMongo

app = flask.Flask(__name__)
app.config["MONGO_URI"] = "mongodb+srv://admin:admin@genshinapi.7wwhf.mongodb.net/genshin?retryWrites=true&w=majority"
mongo = PyMongo(app)
print('initialisation du serveur')


import endpoints.team_by_type.get_team_by_type
import endpoints.team_by_type.get_team_by_character

import endpoints.premade_team.delete_premade_type_and_name
import endpoints.premade_team.get_premade_by_type
import endpoints.premade_team.get_premade_by_type_and_character
import endpoints.premade_team.post_premade_type_and_name
import endpoints.premade_team.put_premade_type_and_name

import endpoints.build.get_artefact
import endpoints.build.get_character
import endpoints.build.get_character_by_type
import endpoints.build.get_weapon

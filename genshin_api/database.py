import json

def implement_database(mongo):
    _implement_team_type_by_element_collection(mongo)
    _implement_team_by_type_collection(mongo)
    _implement_characters_collection(mongo)

def _implement_team_type_by_element_collection(mongo):
    team_type_by_element = mongo.db.team_type_by_element
    
    with open("collections_json/team_type_by_element.json") as team_type_collection:
        data_dict = json.load(team_type_collection)
        for type_character in data_dict:
            team_type_by_element.insert(type_character)

def _implement_team_by_type_collection(mongo):
    team_by_type = mongo.db.team_by_type
    
    with open("collections_json/team_by_type.json") as team_by_type_collection:
        data_dict = json.load(team_by_type_collection)
        for team_by_type_ele in data_dict:
            for key, value in team_by_type_ele.items():  
                team_by_type.insert({key:value})

def _implement_characters_collection(mongo):
    characters = mongo.db.characters
    
    with open("collections_json/build.json") as build:
        list_character = json.load(build)
        for character in list_character:
            characters.insert(character)

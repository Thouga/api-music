from setuptools import setup

setup(
    name='Genshin-API',
    packages=['genshin_api'],
    include_package_data=True,
    install_requires=[
        'flask',
        'Flask-PyMongo',
        'dnspython',
        'requests'
    ],
)
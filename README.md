# API-Music

- Rapide mise en place de flask et de routes très simples.
- Utilisation de swaggerEditor pour documentation de l'API.
- Voir pour implémenter la base de donnée : MongoDB.


# Micro mini tuto d'installation

Déjà ce serait cool d'installer python si tu l'as pas. Vérifie avec la commande : 

```
python --version
```

Si ça marche pas, go google et installe le.

On est en python et en python on fait gaffe à son environnement de travail.
Genre imagine on fait ce projet en python3 et demain tu décides de faire du python2 pour ta culture, il va y avoir des conflits sur tes projets parce que tu n'auras pas la bonne version de python sur ton pc. Du coup pour éviter ça on va créer un environnement de travail virtuel (tkt c'est izi). Tu fais cette petite commande :

```
python -m venv le-ptit-nom-de-ton-choix
```

Ensuite il faut simplement t'assurer que tu l'as activé. Sur vscode, normalement il te demande en bas à droite si tu veux choisir cet environnement sur le dossier de travail. On est flemmard, on dit oui.

Ensuite tu fermes ton petit terminal et tu le réouvre. 
Si tu n'as pas (le-ptit-nom-de-ton-choix) au dessus de ta ligne terminal dans le bash, tu fermes le terminal, tu laches un `CTRL + P` et tu choisis `Python : Select Interpreter`. Maintenant tu ré-ouvres un terminal et magiquement tu es dans environnement de travail.

Comme je suis sympa et que j'ai fais une mini base, tu peux essayer de lancer avec :

```
python api.py
```

Of course ça sort une erreur, il faut que tu prennes Flask. Rien de plus simple avec python : 

```
pip install Flask
```

Et la si tu retente la commande normalement, tu as un joli lien pour checker dans ton navigateur que ça fonctionne.
